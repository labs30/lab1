import math


def main():
    a = float(input(f'Введите значение a: '))
    x = float(input(f'Введите значение x: '))

    g = (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
    f = math.tan(math.radians(5 * a ** 2 + 34 * a * x + 45 * x ** 2))
    d = math.radians(7 * a ** 2 - a * x - 8 * x ** 2)
    if - 1 <= d <= 1:
        y = -1 * math.asin(d)
    else:
        y = 'Ошибка'

    print(f'Даны значения: a = {a} и x = {x}')
    print(f'Ответы: G = {g}, F = {f}, Y = {y}')
    input("Нажмите Enter для завершения")

main()
